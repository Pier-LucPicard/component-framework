'use strict';

const test = require('../index');

const manifest = {
  server: {
    port: 60000,
    host: 'localhost',
  },
  register: {
    plugins: [],
  },
};
test.generateAndStartServer(manifest, {}).then(() => {
  console.log('Running server on localhost:60000');
});
