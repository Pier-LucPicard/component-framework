'use strict';

const Promise = require('bluebird');
const _ = require('lodash');
const path = require('path');
const Glob = Promise.promisify(require('glob'));

async function loadComponents(server, lookupPath, type) {
  const files = await Glob(lookupPath);

  await Promise.each(files, async (file) => {
    const componentName = _.replace(_.replace(`${path.basename(file, '.js')}`, `-${type}`, ''), '-', '_');
    const loadedModule = require(file); // eslint-disable-line global-require

    server.log(['info', 'registration', `${type}`], `Registering the ${type}: ${componentName}`);

    await handleInjection(server, loadedModule, type, componentName);
    const newModule = _.cloneDeep(loadedModule);
    await handleResolve(server, loadedModule, newModule, type, componentName);
    server.expose(componentName, newModule);
  });
}

async function handleResolve(server, loadedModule, newModule, type, componentName) {
  if (loadedModule.resolve) {
    loadedModule.resolve = async (...args) => {
      server.log(['info', `${componentName}`, 'resolve'], `Called with args: ${JSON.stringify(args)}`);
      return newModule.resolve(...args);
    };
  }
}

async function handleInjection(server, loadedModule, type, componentName) {
  if (loadedModule.inject && _.isFunction(loadedModule.inject)) {
    server.log(['info', 'registration', `${type}`], ` -> Injecting the ${type}: ${componentName}`);
    loadedModule.inject(server);
  }
}

module.exports = {
  loadComponents,
};
