'use strict';

const Hoek = require('@hapi/hoek');
const _ = require('lodash');

function getComponentsFactory(server) {
  return async (type, name) => {
    Hoek.assert(_.get(server, `plugins.${type}.${name}`), `"${name}" ${type} is required`);
    return _.get(server, `plugins.${type}.${name}`);
  };
}

module.exports = {
  getComponentsFactory,
};
