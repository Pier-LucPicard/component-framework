'use strict';

const Promise = require('bluebird');
const { loadComponents } = require('./components-loader');
const { getComponentsFactory } = require('./components-getter');
const _ = require('lodash');

async function register(server, options) {
  server.log(['info', 'registration', 'plugins', 'components-loader'], 'Registring Components Loader Plugins');
  server.expose('getComponent', getComponentsFactory(server));

  if (options && options.registrations && _.isArray(options.registrations)) {
    server.log(['info', 'registration', 'plugins', 'components-loader'], 'Found registration');

    await Promise.each(options.registrations, async (registration) => {
      server.log(['info', 'registration'], `Registering the ${registration.name}`);
      await server.register({
        plugin: {
          name: registration.type,
          register: async (internalServer) => {
            await loadComponents(internalServer, registration.path, registration.type);
          },
        },
      });
    });
  }
}

module.exports = {
  register,
  name: 'loading',
};
