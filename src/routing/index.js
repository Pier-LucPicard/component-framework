'use strict';

const { setupRoute } = require('./setup-routing');

async function register(server) {
  server.log(['info', 'registration', 'plugins', 'setup-routing'], 'Registring Components setup-routing Plugins');
  server.expose('setupRoute', setupRoute);
}

module.exports = {
  register,
  name: 'routing',
};
