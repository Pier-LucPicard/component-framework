'use strict';

const Boom = require('@hapi/boom');

async function setupRoute(server, method, path, options, handler) {
  server.log(['info', 'registration', 'api'], `Registering ${method}:${path}`);
  server.route({
    path,
    method,
    handler: async (request, h) => {
      server.log(['http', 'handler'], `Received ${request.method}:${request.path}`);
      let res = {};
      try {
        res = await handler(request, h);
      } catch (err) {
        return Boom.boomify(err, { statusCode: 400 });
      }
      return res;
    },
    options,
  });
}

module.exports = {
  setupRoute,
};
