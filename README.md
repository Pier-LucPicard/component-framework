# Custom Framework


This is a hapi based framework to help create a testable and well divided application server.

This includes server starting setup and some plugins.

Supported plugin
- Loading
- Routing

# Usage
## Server setup
Easy setup
```
const componentFamework = require('componentFamework');

const manifest = {
    server: {
      port: 60000,
      host: 'localhost',
    },
    register: {
      plugins: []
    }
}
componentFamework.generateAndStartServer(manifest, {}).then(() => {console.log('Running server on localhost:60000')});
```
Manual Setup
```
const { generateServer, initServer, startServer } = require('componentFamework');


const manifest = {
    server: {
      port: 60000,
      host: 'localhost',
    },
    register: {
      plugins: []
    }
}

const serverGeneration = async function serverGeneration() {
  try {
    const server = await generateServer(manifest, options);

    await initServer(server);
    await startServer(server);
  } catch (err) {

    console.error(err);
    process.exit(1);
  }
};

serverGeneration().then(() => {console.log('Running server on localhost:60000')});
```
## Plugin Setup

```
const { plugins } = require('componentFamework');

const config = {
    plugins: {
        loading: {
            registrations: [
                { name: 'Schema', type: 'schema', path: `${__dirname}/../server/**/*-schema.js` },
                { name: 'Validator schema', type: 'validator', path: `${__dirname}/../server/**/*-validator.js` }, 
            ],
        },
    }
}

const manifest = {
    server: {
      port: 60000,
      host: 'localhost',
    },
    register: {
      plugins: [
        {
            plugin: plugins.loading,
            options: config.plugins.loading,
        },
        {
            plugin: plugins.routing,
        },
      ]
    }
}

// Now server.plugins.schema[schemaName]
// and
// server.plugins.validator[schemaName]

// Are available
```

# API
## generateAndStartServer(manifest, option, initFunction)
Take a Glue manifest, a Glue option object and an initFunction that will be run after the glue.compose but before the start of the server.

## generateServer(manifest, option)
Take a Glue manifest and a Glue option object.


## initServer(server, initFunction)
Just a hook function that will be run after the glue.compose but before the start of the server.

## startServer(server)
Start the server and start listening.

# Plugins

## loading
The plugin will register every component in the configuration.
### getComponent(server, type, name)
Safe way to get a component registerend from the server.

## routing

### setupRoute(server, method, path, options, handler)
Take the server on which we want to register route, the method of the route, the path of the route, options of the route and the handler function.
