'use strict';

const Glue = require('@hapi/glue');
const _ = require('lodash');

async function generateServer(manifest, options) {
  const server = await Glue.compose(
    manifest,
    options,
  );

  return server;
}

async function initServer(server, initFunction = async () => {}) {
  if (_.isFunction(initFunction)) {
    await initFunction(server);
  }
}

async function startServer(server) {
  await server.start();
  server.log('info', `Server listening on ${server.settings.host}:${server.settings.port}`);
}

module.exports = {
  generateServer,
  initServer,
  startServer,
  generateAndStartServer: async function generateAndStartServer(manifest, options, initFunction) {
    try {
      const server = await this.generateServer(manifest, options);

      await this.initServer(server, initFunction);
      await this.startServer(server);
    } catch (err) {
      console.error(err);
      process.exit(1);
    }
  },
  plugins: {
    loading: require('./src/loading'), // eslint-disable-line global-require
    routing: require('./src/routing'), // eslint-disable-line global-require
  },
};
